package pl.sda.ldz13.springboot.app.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sda.ldz13.springboot.app.model.User;
import pl.sda.ldz13.springboot.app.service.UserService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    private Logger log = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userServiceAnnotationSingleton;

    // każda metoda w kontlorerze musi zwracać nazwę widoku (spring wie gdzie szukac widoków z userController)
    //adres jest mylący (metoda ma tylko i wyłącznić wyświetlić formularz z pustym użytkownikiem)
    @GetMapping(value = "/addUser")
    public String showUserForm(Model model) {
        model.addAttribute("user", new User());
        return "user";
    }

    @PostMapping(value = "/saveUser")
    public String submit(@Valid @ModelAttribute User user, BindingResult result) {

        if (result.hasErrors()) {
            return "user";
        }

        if (user.getId() == null) {
            userServiceAnnotationSingleton.addUser(user);
        } else {
            userServiceAnnotationSingleton.modifyUser(user);
        }

        return "redirect:/users";
    }

    @GetMapping(value = "/users")
    public String showUsers(Model model) {
        List<User> users = userServiceAnnotationSingleton.getAllUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping(value = "/searchUsers")
    public String showUsersWithFilter(Model model, @RequestParam(required = false, defaultValue = "") String name) {

        List<User> users = userServiceAnnotationSingleton.getUsersByName(name);
        model.addAttribute("users", users);
        return "usersWithFilter";
    }

    //w klamrach jest element dynamiczny w adresie
    @GetMapping(value = "user/{id}/update")
    //PathVariable to adnotacja aby uzupełniło scieżkę w adresie
    public String showUserToModify(Model model, @PathVariable Long id) {
        model.addAttribute("user", userServiceAnnotationSingleton.getUserById(id));
        return "user";
    }

    @GetMapping(value = "user/{id}/delete")
    public String deleteUser(Model model, @PathVariable Long id) {
        userServiceAnnotationSingleton.deleteUser(id);

        userServiceAnnotationSingleton.getAllUsers();
        model.addAttribute("users", userServiceAnnotationSingleton.getAllUsers());

        return "redirect:/searchUsers";
    }

}
