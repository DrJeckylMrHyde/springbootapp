package pl.sda.ldz13.springboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.springboot.app.model.User;

import java.util.List;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {


    List<User> findByNameIgnoreCaseContaining(String name);

    //trzeba oznaczyć atrybuty meteody adnotacją @Param i to się uzywa w zapytaniu po dwukropku (tabela w zapytaniu to tak naprawde
    // nasza klasa user, a kolumny to w zapytaniu tak naprawde pola w klasie)
    @Query("select user from pl.sda.ldz13.springboot.app.model.User user where user.name = :name and user.age = :age")
    //to zapytanie poniżej jest równoważne i pisane jest w natywnym SQLu
    //@Query(nativeQuery = true, value = "select * from user_entity where name = :name and age = :age")
    List<User> findNameAndAge(@Param("name") String name, @Param("age") int age);
}
