package pl.sda.ldz13.springboot.app.exeptions;

public class UserNotFoundExeption extends RuntimeException {
    public UserNotFoundExeption(String message){
        super(message);
    }
}
