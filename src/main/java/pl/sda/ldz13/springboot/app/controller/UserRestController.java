package pl.sda.ldz13.springboot.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pl.sda.ldz13.springboot.app.exeptions.UserNotFoundExeption;
import pl.sda.ldz13.springboot.app.model.User;
import pl.sda.ldz13.springboot.app.service.UserService;

import java.util.List;

@RestController
@RequestMapping("rest/user")
public class UserRestController {

    //dostarczenie obiektu z kontekstu springa (mówie ze dostane obiekt userService w swojej klasie)
    //jesli ustawimy pole (final) to nie musimy pisać @Autowired - musimy wtedy zdefiniować konstruktor
    //jests to przykład prawidłowego wstrzyknięcia zależności
    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/save")
    //RequestBody zeby parsował JSONA na obiekt
    public ResponseEntity<User> save(@RequestBody User user) {
        User userAdded = userService.addUser(user);
        //metoda .ok zwraca kod 200 czyli ok
        return ResponseEntity.ok(userAdded);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<User>> getAll() {
        List<User> allUsers = userService.getAllUsers();
        return ResponseEntity.ok(allUsers);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        try {
            User userById = userService.getUserById(id);
            return ResponseEntity.ok(userById);
        } catch (UserNotFoundExeption ex) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "User not found", ex
            );
        }
    }

    @PutMapping("/update")
    public ResponseEntity<User> modify(@RequestBody User user) {
        User modifyUser = userService.modifyUser(user);
        return ResponseEntity.ok(modifyUser);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        boolean isDeleted = userService.deleteUser(id);
        return isDeleted ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }

    @GetMapping("/findByName")
    public ResponseEntity<List<User>> findByName(@RequestParam String name) {
        List<User> usersByName = userService.getUsersByName(name);
        return ResponseEntity.ok(usersByName);
    }

    @GetMapping("/findByNameAge")
    //responseEntity zwraca kody html
    public ResponseEntity<List<User>> findByNameAndAge(@RequestParam String name, @RequestParam int age) {
        List<User> usersByNameAndAge = userService.getUsersByNameAndAge(name, age);
        return ResponseEntity.ok(usersByNameAndAge);
    }
}
